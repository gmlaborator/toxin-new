<?php

if(isset($_POST["email"]) && isset($_POST["telefon"]) && isset($_POST["poznamka"]) && isset($_POST["tag"]) && isset($_POST["jmeno"]) && isset($_POST["token"]) && isset($_POST["subjectType"])){
  $captcha_secret = "6LeUg8MUAAAAAD66DMmytzPb7rCme3G29-QNpabv";
  $captcha = $_POST["token"];
  $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$captcha_secret&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
  if($response['success'] == true){

    $error = false;
    $error_message = "";

    if($_POST["tag"] == "popup" || $_POST["tag"] == "page"){
        $tag = $_POST["tag"];
        $tag_valid = true;
    }else{
        $error = true;
        $error_message .= "Invalid tag\n";
    }

    $jmeno  = htmlspecialchars(stripslashes(trim($_POST['jmeno'])));
    $email  = htmlspecialchars(stripslashes(trim($_POST['email'])));
    $telefon = htmlspecialchars(stripslashes(trim($_POST['telefon'])));
    $poznamka = htmlspecialchars(stripslashes(trim($_POST['poznamka'])));
    $subjectType = htmlspecialchars(stripslashes(trim($_POST['subjectType'])));


    if(!preg_match("/^[+]?[0-9 -]{9,12}/", $telefon)){
        $error_message .= 'Nepatné telefonní číslo<br>';
        $error = true;
    }

    if(!preg_match("/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/", $email)){
        $error_message .= 'Neplatný email <br>';
        $error = true;
    }


    /*
    if(strlen($poznamka) === 0){
        $error_message .= 'Zpráva nesmí být prázdná<br>';
        $error = true;
    }
*/

    if(!$error){
        require_once "../class/Mail.php";
        $mail = new Mail();
        $subject = "Žádost o vyzkoušení systému Maximus";
        if($subjectType == 1){
          $subject = "Žádost o vyzkoušení systému Maximus";
        }
        if($subjectType == 2){
          $subject = "Konzultace systému Maximus";
        }
        if($subjectType == 3){
          $subject = "Poptávka analýzy monitoringu médií";
        }
            $text = "Jméno: ".$jmeno. "\n"
                   ."Email: ".$email. "\n"
                   ."Telefon: ".$telefon. "\n"
                   ."Poznámka: " .$poznamka;
        /*
        else{

            $subject = "Žádost o vyzkoušení systému Maximus";
            $text = "Jméno: ".$jmeno. "\n"
            ."Email: ".$email. "\n"
            ."Telefon: ".$telefon. "\n"
            ."Poznamka: ".$poznamka. "\n";
        }
*/


        $odeslani = $mail->sendMail($subject, $text, $email);
        $vysledek = Array("class" =>"success", "message" => "Zpráva byla odeslána! Výsledek: $odeslani");
        $vysledek = json_encode($vysledek);
    }


    else{
        $vysledek = Array("class" =>"error", "message" => $error_message);
        $vysledek = json_encode($vysledek);
    }


  }
  else{
    $vysledek = Array("class" =>"error", "message" => "Ochrana proti botu zablokovala požadavek");
    $vysledek = json_encode($vysledek);
  }
    echo $vysledek;
}
