<?php
class Mail {
    private $emails;

    function __construct() {
        //Pole s emailovými adresami. Pro odeslání emailu na více adres stačí adresy doplnit do pole.
        $this->emails = Array(
            "radex118@gmail.com",
            "katerina.bilkova@vse.cz",
            "zdenek.vondra@vse.cz",
            "voldrichma@gmail.com",
            "info@toxin.cz",
        );
    }


    //Funkce pro odesílání emailů na všechny adresy uvedení v poli $emails
    public function sendMail($subject, $text, $reply) {
        $error = false;
        if($subject != "" && $text != ""){
            for($i = 0; $i < count($this->emails); $i++){
                $headers = 'From:  Kontaktní formulář  - Toxin.cz  <info@toxin.cz>' . "\r\n" .
                'Reply-To: '. $reply . "\r\n" .
                "MIME-Version: 1.0"."\r\n".
                "Content-Type: text/html; charset=UTF-8"."\r\n";
                $target = $this->emails[$i];
                if(!mail($target, $subject, $text, $headers)){
                    $error = true;
                }
            }
            return $error;
        }else{
            return false;
        }
    }

}
