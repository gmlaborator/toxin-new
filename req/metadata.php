<!-- Google Tag Manager -->
<script type="text/plain" data-cookiecategory="analytics">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P2GQBFC');</script>
<!-- End Google Tag Manager -->

<!-- Google Tag Manager (noscript) -->
<noscript ><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2GQBFC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<script type="text/plain" data-cookiecategory="analytics">
  window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '74a6639632808afd4025ae54504f91356ee4b49d');
</script>



<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Maximus je nejrychlejší inteligentní monitoring médií od společnosti Toxin.">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="author" content="Martin Voldřich" />
<meta name="format-detection" content="telephone=no">


<link type="text/plain" rel="author" href="https://toxin.cz/humans.txt" />
<meta name="robots" content="index,follow" />
<meta name="googlebot" content="index, follow" />
<meta name="keywords" content="inteligentní monitoring médií, Toxin, Maximus, Modulární systém, analýza, ČTK" />

<meta property="og:title" content="Maximus - Inteligentní monitoring médií">
<meta property="og:type" content="Maximus je nejrychlejší inteligentní monitoring médií od společnosti Toxin.">

  <meta property="og:image" content="https://toxin.cz/images/ico/share_facebook.png"> 
  <meta property="og:url" content="https://toxin.cz/index.php"> 

<meta property="og:description" content="Maximus - Inteligentní monitoring médií">

<!-- Disable tap highlight on IE -->
<meta name="msapplication-tap-highlight" content="no">

<!-- Web Application Manifest -->
<link rel="manifest" href="manifest.json">

<!-- Add to homescreen for Chrome on Android -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="Maximus - Inteligentní monitoring médií">
<link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png">

<!-- Add to homescreen for Safari on iOS -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="Maximus - Inteligentní monitoring médií">
<link rel="apple-touch-icon" href="images/touch/apple-touch-icon.png">

<!-- Tile icon for Win8 (144x144 + tile color) -->
<meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#1e2d8e">

<!-- Color the status bar on mobile devices -->
<meta name="theme-color" content="#121966">

<!-- Le fav and touch icons -->
<link rel="apple-touch-icon" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon" href="images/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="images/ico/favicon.png">