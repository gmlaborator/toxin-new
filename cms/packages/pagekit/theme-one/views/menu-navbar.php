<?php if ($root->getDepth() === 0) : ?>
<ul class="uk-navbar-nav">

<?php endif ?>


<li class="uk-parent" >
    <a href="/">Monitoring médií Maximus</a>
</li>

<li class="uk-parent" >
    <a href="/">Analýza monitoringu médií</a>
</li>

<li class="uk-parent" >
    <a href="/">O nás</a>
</li>
<li class="uk-parent" >
    <a href="/">Kontakt</a>
</li>
<li class="uk-parent" >
    <a href="/" class="navbar-toxin-menu-button">Přihlásit do Maximu</a>
</li>

<?php if ($root->getDepth() === 0) : ?>
</ul>
<?php endif ?>
