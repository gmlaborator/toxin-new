<!doctype html>
<html lang="">
   <head>

   <?php include_once('req/metadata.php'); ?>
      <title>Toxin</title>
      <link rel="stylesheet" href="css/main.css">
      <link rel="stylesheet" href="css/animateModal/animate.min.css">


      <script src="https://www.google.com/recaptcha/api.js?render=6LeUg8MUAAAAANNNxn90gY7IS-l9CaxNhroGf4Ni"></script>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="scripts/jquery-1.9.1.min.js"><\/script>')</script>
      <script src="scripts/animateModal/animatedModal.js"></script>
      <script src="scripts/main.js" type="text/javascript"></script>
      <script src="scripts/nicescroll.js"></script>
      <script src="scripts/venobox.js"></script>

   </head>
   <body>
      <div class="menu">
      <div class="menu__inside">
            <a href="#header" class="menu__inside__logo">
               <div class="menu__inside__logo__image"></div>
            </a>


            <div class="menu__inside__socials">
               <a href="https://www.linkedin.com/company/toxin-s-r-o/" target="_blank" class="menu__inside__socials__link">
                  <div class="menu__inside__socials__link__linkedin"></div>
               </a>
               <a href="https://twitter.com/CzToxin" target="_blank" class="menu__inside__socials__link">
                  <div class="menu__inside__socials__link__twitter"></div>
               </a>
               <a href="https://www.facebook.com/firma.toxin" target="_blank" class="menu__inside__socials__link">
                  <div class="menu__inside__socials__link__facebook"></div>
               </a>
            </div>


         <div class="menu__inside__links">
         <ul class="navigace__ul">
         <li> <a href="#maximus" class="menu__inside__links__link" id="navigace-prvni">Monitoring médií Maximus</a> </li>
         <li> <a href="#analyza_monitoringu_medii" class="menu__inside__links__link" id="navigace-prvni">Analýza monitoringu médií</a> </li>
         <li> <a href="#oNas" class="menu__inside__links__link" id="navigace-druhe">O nás</a> </li>
         <li>  <a href="#kontakt" class="menu__inside__links__link" id="navigace-treti">Kontakty</a> </li>
         <li>  <a href="/blog" class="menu__inside__links__link" id="navigace-treti">Blog</a> </li>
         <li> <a href="https://www.maximusweb.cz/" target="_blank" class="menu__inside__links__link mainOne">Přihlásit do Maximu</a> </li>
          <a id="zavrit"></a>
          </ul>

         </div>



            <div class="menu__inside__hamburger" id="js-hamburger">
               <div class="menu__inside__hamburger__icon"></div>
            </div>

         </div>
      </div>

      <a href="#" class="chatIcon">
         <div class="chatIcon__image"></div>
         <div class="chatIcon__button">
            <span></span>
         </div>
      </a>






<div class="validace_success" id="message-popup-ajax-success">
<div class="validace_success__firstbox">
<p>Úspěšně odesláno!</p>
</div>
<div class="validace_success__secondbox">
<p>Brzy Vás kontaktuje kolegyně nebo kolega z našeho týmu..</p>
</div>
<span class="validace_success__zavrit"></span>
</div>



<div class="validace_error" id="message-popup-ajax-error" >
<div class="validace_error__firstbox">
<p>Je třeba vyplnit všechna pole</p>
</div>
<div class="validace_error__secondbox">
<p id="error-text">Prosím vyplňte pole jméno.</p>
</div>
<span class="validace_error__zavrit"></span>
</div>




  <div id="js-popup" class="popup_formular">
  <div class="modal-content">
  <div  id="btn-close-modal" class="close-js-popup close_popup">Zavřít</div>
  <div class="modal-content__form">


 <div class="modal-content__form__first">
 <p>Děkujeme za Váš zájem. Prosíme vyplňte níže uvedené údaje. Následně Vás budeme kontaktovat a&nbsp;projdeme s&nbsp;Vámi vhodné řešení. Budeme k&nbsp;dispozici pro zodpovězení jakýchkoliv dotazů. </p>
 </div>




  <div class="modal-content__form__formular">




 <form method="post" id="popup-formular-input" lang="cs" >

 <div class="modal-content__form__formular__box1"><input name="email" type="email" class="modal-content__form__form" placeholder="Jméno" id="popup-jmeno"  />   </div>
 <div class="modal-content__form__formular__box2"><input name="telefon" type="text" class="modal-content__form__form" placeholder="Telefon" id="popup-telefon" /></div>
 <div class="modal-content__form__formular__box3"> <input name="email" type="text" class="modal-content__form__form" placeholder="Email" id="popup-email"  /></div>
 <div class="modal-content__form__formular__box4">

<input type="text" hidden="hidden" id="popup-tagPopup" name="tag">
<input name="poznamka" type="text" class="modal-content__form__form" placeholder="Poznámka" id="popup-poznamka"   />

 </div>


 <div class="modal-content__form__formular__errors" id="display-error-box-popup"></div>
 <div id="popup-odeslatFormularEmail" class="modal-content__form__formular__button" >Odeslat</div>
 </form>


 </div>

 </div>
 </div>
 </div>




      <div class="firstBlock" id="header" >
         <div class="firstBlock__inside">

            <div class="firstBlock__inside__right">
               <div class="firstBlock__inside__right__tablet"></div>
               <div class="firstBlock__inside__right__icons">
                  <div class="firstBlock__inside__right__icons__icon">
                     <div class="firstBlock__inside__right__icons__icon__image">
                        <div class="prvni"></div>
                     </div>
                     <h2>První u&nbsp;zdroje</h2>
                  </div>
                  <div class="firstBlock__inside__right__icons__icon">
                     <div class="firstBlock__inside__right__icons__icon__image">
                        <div class="alert"></div>
                     </div>
                     <h2>Alert do tří minut</h2>
                  </div>

                  <div class="firstBlock__inside__right__icons__icon">
                     <div class="firstBlock__inside__right__icons__icon__image">
                        <div class="modularni"></div>
                     </div>
                     <h2>Modulární systém</h2>
                  </div>

                  <div class="firstBlock__inside__right__icons__icon">
                     <div class="firstBlock__inside__right__icons__icon__image">
                        <div class="pohodli"></div>
                     </div>
                     <h2>Pohodlí uživatele</h2>
                  </div>




               </div>
            </div>

            <div class="firstBlock__inside__left">


               <div class="firstBlock__inside__left__text">
                  <strong>Maximus</strong> je nejrychlejší <span></span> inteligentní <strong>monitoring médií</strong><span></span> od společnosti <strong>Toxin</strong>.
               </div>

               <a href="#maximus" class="firstBlock__inside__left__button">
               Systém Maximus
               </a>

            </div>
         </div>
      </div>



      <div class="secondBlock" id="maximus">
         <div class="secondBlock__inside">
            <div class="secondBlock__inside__logo"></div>
            <h1>Monitoring médií Maximus</h1>
            <div class="secondBlock__inside__firstText">
             Sledujte, jak o&nbsp;vás mluví v médiích. Komunikujte včas a&nbsp;správnému publiku. Reagujte na diskuse a&nbsp;uvádějte informace na pravou míru. Pohotově chraňte svou značku, monitorujte konkurenci a&nbsp;udržujte si svoji reputaci pod kontrolou. Řešte krizové situace s&nbsp;předstihem, protože dobré i&nbsp;špatné zprávy budete vědět jako první a&nbsp;získáte tak čas na důležitá rozhodnutí. Zaměřte se tak na nové trhy pro vaše služby a&nbsp;produkty. Sledujte vývoj vašeho odvětví.
            </div>



            <div class="secondBlock__inside__secondText">
            Maximus je systém pro <strong>nepřetržité sledování</strong> televize, rozhlasu, tisku, on-line zpravodajských serverů a&nbsp;sociálních sítí. Jeho <strong>umělá inteligence</strong> dokáže monitorovat již 42 celostátních a&nbsp;regionálních kanálů televizního a&nbsp;rozhlasového vysílání. Systém jako jediný <strong>spolupracuje s&nbsp;ČTK</strong> a&nbsp;umožňuje tak získávání zpráv nejrychleji a&nbsp;přímo od zdroje.
            </div>




            <div class="secondBlock__inside__secondText">
            Systém funguje na základě sledování definovaných klíčových slov a&nbsp;výrazů. Okamžitě upozorní na každou sledovanou informaci a&nbsp;to nejpozději <strong>do 3&nbsp;minut od&nbsp;zveřejnění zprávy</strong> a&nbsp;s&nbsp;odstraněním duplicit. Výsledek dostanete v&nbsp;podobě přehledného reportu, jehož strukturu si sami zvolíte. Zprávy můžete neomezeně procházet v&nbsp;revolučním rozhraní.
            </div>





            <div class="secondBlock__inside__points">
               <div class="secondBlock__inside__points__point">
                  <div class="secondBlock__inside__points__point__image">
                     <div class="prvni"></div>
                  </div>
                  <h2>První u zdroje</h2>
                  <p>Pomocí umělé inteligence a&nbsp;zpracování tiskových konferencí ČTK v&nbsp;reálném čase.</p>




               </div>
               <div class="secondBlock__inside__points__point">
                  <div class="secondBlock__inside__points__point__image">
                     <div class="alert"></div>
                  </div>
                  <h2>Alert do tří minut</h2>
                  <p>Informaci o&nbsp;nové zmínce získáte nejrychleji na českém trhu.</p>



               </div>


               <div class="secondBlock__inside__points__point">
                  <div class="secondBlock__inside__points__point__image">
                     <div class="maximus"></div>
                  </div>
                  <h2>Maximus je modulární</h2>
                  <p>Řešení na základě vašich potřeb a&nbsp;nebo doplnění vašeho stávajícího systému.</p>
               </div>


               <div class="secondBlock__inside__points__point">
                  <div class="secondBlock__inside__points__point__image">
                     <div class="pohodli"></div>
                  </div>
                  <h2>Pohodlí uživatele</h2>
                  <p>Díky propracovanému rozhraní a&nbsp;zákaznické podpoře od&nbsp;obchodníků i&nbsp;back office.</p>
               </div>


               <div class="secondBlock__inside__thirdText">
               Systém Maximus je možné vyzkoušet bezplatně, kontaktujte nás.
               </div>




               <div class="secondBlock__inside__box">
               <a href="#js-popup" onClick="setTag(1)" class="secondBlock__inside__box__button formular">Poptat Maximus</a>
               </div>






            </div>


            </div>
         </div>








      </div>

      <div class="thirdBlock">
         <div class="thirdBlock__inside">
            <div class="thirdBlock__inside__maximus"></div>
            <div class="thirdBlock__inside__secondLine">a jeho moduly</div>
            <div class="thirdBlock__inside__boxes">


               <div class="thirdBlock__inside__boxes__center">
                  <div class="thirdBlock__inside__boxes__center__box">
                     <div class="thirdBlock__inside__boxes__center__box__toxin"></div>
                     <h2>xStream (TVR)</h2>
                     <p>Automatizovaný přepis televizního vysílání a&nbsp;rádia postavený na&nbsp;umělé inteligenci s&nbsp;uživatelsky nejdokonalejším rozhraním umožňujícím pohodlné procházení záznamů i&nbsp;pochopení kontextu zmínky.</p>
                  </div>
               </div>


               <div class="thirdBlock__inside__boxes__box">
                  <div class="thirdBlock__inside__boxes__box__toxin"></div>
                  <h2>Tiskové konference ČTK</h2>
                  <p>Jediný nástroj na&nbsp;českém trhu, který umí automatizovaně zpracovávat tiskové konference ČTK a&nbsp;posílat z&nbsp;nich media alerty. Tiskové konference ČTK jsou prvním místem publikování zpráv s&nbsp;několika hodinovým předstihem než je&nbsp;zpracují redaktoři.</p>
               </div>


               <div class="thirdBlock__inside__boxes__box">
                  <div class="thirdBlock__inside__boxes__box__toxin"></div>
                  <h2>DATEL (ruční přepis TV, rádia a&nbsp;tisku)</h2>
                  <p>Modul systému Maximus pro&nbsp;ručně zpracovaný přepis obsahu ve&nbsp;sledovaných televizích, rádiích a&nbsp;tisku.</p>
               </div>
               <div class="thirdBlock__inside__boxes__box">
                  <div class="thirdBlock__inside__boxes__box__toxin"></div>
                  <h2>Modul sociální sítě</h2>
                  <p>Automatizované monitorování a&nbsp;přepis obsahu sociálních sítí.</p>
               </div>
               <div class="thirdBlock__inside__boxes__box">
                  <div class="thirdBlock__inside__boxes__box__toxin"></div>
                  <h2>Modul online zpravodajství</h2>
                  <p>Automatizovaný monitoring a&nbsp;přepis online zpravodajství a&nbsp;obsahových sítí.</p>



               </div>
               <div class="thirdBlock__inside__boxes__box">
                  <div class="thirdBlock__inside__boxes__box__ctk">05</div>
                  <h2>Zpravodajství ČTK</h2>
                  <p>Modul integruje službu ČTK pro přímé poskytování tiskových zpráv do&nbsp;systému Maximus. Získávejte zprávy přímo z&nbsp;ČTK a&nbsp;nečekejte, až&nbsp;o&nbsp;nich redaktoři napíší.</p>


               </div>

               <div class="thirdBlock__inside__boxes__box">
                  <div class="thirdBlock__inside__boxes__box__toxin"></div>
                  <h2>Modul tisk</h2>
                  <p>Automatizovaný monitoring upozorní na zprávy z tisku, které vyjdou zítra ráno.</p>



               </div>


            </div>
         </div>
      </div>

      <div class="fourthBlock" >
         <div class="fourthBlock__left">
            <div class="fourthBlock__left__inside">
               <h2>Práce se systémem</h2>
               <p>V každém kraji České republiky máme svého vysoce kvalifikovaného pracovníka, který se vám bude osobně věnovat. Je připraven vám celý systém kdykoli podrobně představit, nastavit, zaškolit vaše pracovníky či&nbsp;pomoci s&nbsp;přípravou důležitých dokumentů.</p>
               <p>Naši konzultanti jsou všem stávajícím klientům nepřetržitě k&nbsp;dispozici pro&nbsp;jakékoliv ad&nbsp;hoc požadavky. Zakládáme si na&nbsp;úspoře vašeho času a&nbsp;na&nbsp; vysoké míře personifikace systému. Každému uživateli přizpůsobíme služby přesně na&nbsp;míru, dle jeho individuálních potřeb.</p>


               <a href="#js-popup" class="fourthBlock__left__inside__button formular"  onClick="setTag(2)" >Konzultovat Maximus</a>



            </div>
         </div>
         <div class="fourthBlock__right">
            <div class="fourthBlock__right__inside">



            <div class="fourthBlock__right__video">




    <video class="projekty" autoplay loop muted playsinline preload poster="">
    <source src="video/video_maximus.webm" type="video/webm" >
    <source src="video/video_maximus.mp4" type="video/mp4">
    Omlouváme se, Váš prohlížeč nepodporuje HTML5 video.
    </video>



            </div>





            </div>
         </div>
      </div>




      <div class="fifthBlock">
         <div class="fifthBlock__inside">
            <h2>Pro koho je <span></span> Maximus určen</h2>
            <div class="fifthBlock__inside__boxes">



            <div class="fifthBlock__inside__boxes__box">
                  <div class="velke"></div>
                  <h3>Tiskový mluvčí, <span></span>&nbsp;Public Relations</h3>
               </div>

               <div class="fifthBlock__inside__boxes__box">
                  <div class="organizace"></div>
                  <h3>
                  Manažer PR, <span></span> komunikace,&nbsp;marketingu
                  </h3>
               </div>

               <div class="fifthBlock__inside__boxes__box">
                  <div class="stredni"></div>
                  <h3>Analytik <span></span> komunikace a PR</h3>
               </div>


            </div>


            <div class="fifthBlock__inside__firstBlock">
            <p>Maximus je určen pro všechny, kdo potřebují vědět o&nbsp;aktuálním dění. Využívají ho malé, střední i velké společnosti, státní organizace a&nbsp;instituce.</p>

            </div>


            <div class="fifthBlock__inside__secondBlock">
            <ul>
   <li>Banky a pojišťovny</li>
   <li>Kraje a města</li>
   <li>Ministerstva</li>
   <li>Neziskové organizace</li>
   <li>Obchodní řetězce</li>
   <li>Reklamní a PR agentury</li>
   <li>Střední a vysoké školy</li>
   <li>Svazy a asociace</li>
   <li>Vědecké i&nbsp;správní instituce</li>
   <li>Zdravotnická zařízení</li>
   </ul>


<ul>
<li>Další firmy, kterým se vyplatí sledovat&nbsp;zprávy</li>
</ul>





            </div>
         </div>
      </div>


      <div class="reference">
         <div class="reference__inside">
      <h2>Vybrané reference, kde Maximus pomohl</h2>




             <div class="reference__inside__images">
                <div class="reference__inside__images__image">
                  <div class="Nmuseum"></div>
                </div>

                <div class="reference__inside__images__image">
                 <div class="electroworld"></div>
                </div>


                <div class="reference__inside__images__image">
                  <div class="geosangroup"></div>
                </div>

                <div class="reference__inside__images__image">
                 <div class="kralovehradeckykraj"></div>
                </div>



                <div class="reference__inside__images__image">
                  <div class="nemocnicetomasebati"></div>
              </div>


                <div class="reference__inside__images__image">
                 <div class="eltodo"></div>
               </div>

               <div class="reference__inside__images__image">
                  <div class="libereckykraj"></div>
                </div>


             </div>
          </div>
          </div>



   <div class="dalsi_sluzby" id="analyza_monitoringu_medii">
   <div class="dalsi_sluzby__firstBOX">



   <h2>Analýza <span></span>monitoringu médií</h2>



   <p>Toxin nabízí vedle systému pro mediální monitoring Maximus také zpracování personalizované analýzy monitoringu médií. Naši PR analytici a&nbsp;konzultanti vám pomohou se hlouběji ponořit do vašich dat, objevit trendy, rizika a&nbsp;příležitosti. Sledujeme klíčové ukazatele jako obor média, mediatyp, AVE hodnotu, čtenost, rozsah výstupu, formát výstupu, periodicitu, odkazy, výskyt cen, výskyt log, výskyt fotek, výskyt citací, tonalitu, zaměření, zásah v&nbsp;mediatypech, zásah v&nbsp;dané cílové skupině, počet GRPů a&nbsp;TRPů za kampaň či její část, ukazatel&nbsp;OTS&nbsp;atd.</p>



   <ul>
   <li class="medialni_obraz">Zjistíme, jaký je váš mediální obraz, zda v&nbsp;něm převažuje pozitivní či&nbsp;negativní kontext.</li>
   <li class="srovnani">Provedeme srovnání mediálního obrazu s&nbsp;konkurencí.</li>
   <li class="vyhodnoceni">Vyhodnotíme mediální dopad kampaní.</li>
   <li class="navsteva">Zpracujeme data pro zlepšení návštěvnosti webových stránek&nbsp;a&nbsp;sociálních sítí.</li>
   <li class="prezentace">Zjistíme, kde se nejvíce prezentuje nejúspěšnější konkurence a v jakých médiích byste měli inzerovat.</li>
   <li class="zprava">Připravíme analýzy určené pro výroční zprávy.</li>
   </ul>



   <p class="subtext">Analýzu vám nejen zpracujeme, ale závěry odprezentujeme a&nbsp;navrhneme další doporučený postup.</p>












   <div class="dalsi_sluzby__buttonBOX">
   <a href="#js-popup" class="formular" onClick="setTag(3)" >Poptat analýzu</a>

   </div>


   </div>


   <div class="dalsi_sluzby__secondBOX">
      <div class="grafy"></div>

   </div>
   </div>


   <div class="form" id="kontakt">

   <div class="form__kontakt">
   <h2>Kontaktujte našeho <span></span>konzultanta</h2>


   <p>+420 777 706 476</p>
   <p class="protectedEmail" title="info@toxin.cz"></p>

   </div>

   <div class="form__form">


   <form method="post" id="formular-input" lang="cs" >

   <input name="jmeno" type="jmeno" class="form__form__cela" placeholder="Jméno" id="jmeno"  />


   <input name="email" type="email" class="form__form__polovina" placeholder="Email" id="email"  />
   <input name="telefon" type="text" class="form__form__polovina" placeholder="Telefon" id="telefon" />


   <input name="poznamka" type="text" class="form__form__cela" placeholder="Poznámka" id="poznamka"  />
      <div class="form__form__errors" id="display-error-box"></div>
   <div id="odeslatFormularEmail" class="form__form__button" >Odeslat</div>

   </form>
   </div>
   </div>


      <div class="footer" id="oNas">
      <h2></h2>

      <p>Společnost se zabývá vývojem a&nbsp;distribucí nástroje pro správu informačních zdrojů Maximus, analýzou monitoringu médií, audiovizuálními přepisy a&nbsp;odvozenými službami. Klíčovým zájmem firmy je dlouhodobý rozvoj vlastní umělé inteligence integrující moderní existující řešení. Orientujeme se na přesnost zpracování dat, rychlost doručení upozornění a&nbsp;uživatelskou zkušenost při práci s&nbsp;našimi produkty.</p>
      <p>TOXIN s.&nbsp;r.&nbsp;o. byl založen v&nbsp;roce 2013 a&nbsp;je součástí české skupiny firem společně s&nbsp;DC GROUP, spol. s&nbsp;r.o., Marketingové databáze, s.r.o., SOLIDIS s.r.o.&nbsp;a&nbsp;INFOPROFI GROUP&nbsp;s.r.o.</p>

      <div class="footer__about">
      <h5>TOXIN s.r.o.</h5>
      <p>Zapsaná v&nbsp;OR&nbsp;vedeném Městským soudem v&nbsp;Praze, oddíl&nbsp;C, vložka 217163, IČ&nbsp;02225166, DIČ&nbsp;CZ02225166, Bankovní spojení: 22333228/5500.</p>
      <p>Vídeňská&nbsp;545/76, 148&nbsp;00 Praha&nbsp;4 – Kunratice.</p>
      </div>


      <div class="footer__box">

      <div class="footer__kontakt">

      <div class="footer__kontakt__tel">
      <span>+420 777 706 476</span>
      </div>

      <div class="footer__kontakt__email">
      <span class="protectedEmail" title="info@toxin.cz"></span>
      </div>
      </div>


      <div class="footer__kontakt__social">
      <div class="footer__kontakt__social__ikony">
      <a href="https://www.linkedin.com/company/toxin-s-r-o/" target="_blank" class="footer__kontakt__social__ikony__in"></a>
      <a href="https://twitter.com/CzToxin" target="_blank" class="footer__kontakt__social__ikony__tw"></a>
      <a href="https://www.facebook.com/firma.toxin" target="_blank" class="footer__kontakt__social__ikony__fb"></a>
      </div>
      </div>
      </div>
      </div>

     </div>


   </body>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/orestbida/cookieconsent@v2.7.1/dist/cookieconsent.js"></script>
   <script>
        window.addEventListener('load', function(){
            
            // obtain plugin
            var cc = initCookieConsent();

            // run plugin with your configuration
            cc.run({
                current_lang: 'cs',
                autoclear_cookies: true,                   // default: false
                theme_css: 'https://cdn.jsdelivr.net/gh/orestbida/cookieconsent@v2.7.1/dist/cookieconsent.css',  
                page_scripts: true,                        // default: false

                // delay: 0,                               // default: 0
                // auto_language: '',                      // default: null; could also be 'browser' or 'document'
                // autorun: true,                          // default: true
                // force_consent: false,                   // default: false
                // hide_from_bots: false,                  // default: false
                // remove_cookie_tables: false             // default: false
                // cookie_name: 'cc_cookie',               // default: 'cc_cookie'
                // cookie_expiration: 182,                 // default: 182 (days)
                // cookie_necessary_only_expiration: 182   // default: disabled
                // cookie_domain: location.hostname,       // default: current domain
                // cookie_path: '/',                       // default: root
                // cookie_same_site: 'Lax',                // default: 'Lax'
                // use_rfc_cookie: false,                  // default: false
                // revision: 0,                            // default: 0

                onFirstAction: function(user_preferences, cookie){
                    // callback triggered only once on the first accept/reject action
                },

                onAccept: function (cookie) {
                    // callback triggered on the first accept/reject action, and after each page load
                },

                onChange: function (cookie, changed_categories) {
                    // callback triggered when user changes preferences after consent has already been given
                },

                languages: {
                  'en': {
                        consent_modal: {
                            title: 'We use cookies!',
                            description: 'Hi, this website uses essential cookies to ensure its proper operation and tracking cookies to understand how you interact with it. The latter will be set only after consent. <button type="button" data-cc="c-settings" class="cc-link">Let me choose</button>',
                            primary_btn: {
                                text: 'Accept all',
                                role: 'accept_all'              // 'accept_selected' or 'accept_all'
                            },
                            secondary_btn: {
                                text: 'Reject all',
                                role: 'accept_necessary'        // 'settings' or 'accept_necessary'
                            }
                        },
                        settings_modal: {
                            title: 'Cookie preferences',
                            save_settings_btn: 'Save settings',
                            accept_all_btn: 'Accept all',
                            reject_all_btn: 'Reject all',
                            close_btn_label: 'Close',
                            cookie_table_headers: [
                                {col1: 'Name'},
                                {col2: 'Domain'},
                                {col3: 'Expiration'},
                                {col4: 'Description'}
                            ],
                            blocks: [
                                {
                                    title: 'Cookie usage 📢',
                                    description: 'I use cookies to ensure the basic functionalities of the website and to enhance your online experience. You can choose for each category to opt-in/out whenever you want. For more details relative to cookies and other sensitive data, please read the full <a href="#" class="cc-link">privacy policy</a>.'
                                }, {
                                    title: 'Strictly necessary cookies',
                                    description: 'These cookies are essential for the proper functioning of my website. Without these cookies, the website would not work properly',
                                    toggle: {
                                        value: 'necessary',
                                        enabled: true,
                                        readonly: true          // cookie categories with readonly=true are all treated as "necessary cookies"
                                    }
                                }, {
                                    title: 'Performance and Analytics cookies',
                                    description: 'These cookies allow the website to remember the choices you have made in the past',
                                    toggle: {
                                        value: 'analytics',     // your cookie category
                                        enabled: false,
                                        readonly: false
                                    },
                                    cookie_table: [             // list of all expected cookies
                                        {
                                            col1: '^_ga',       // match all cookies starting with "_ga"
                                            col2: 'google.com',
                                            col3: '1 year',
                                            col4: 'Google Analytics',
                                            is_regex: true
                                        }
                                    ]
                                }, {
                                    title: 'Advertisement and Targeting cookies',
                                    description: 'These cookies collect information about how you use the website, which pages you visited and which links you clicked on. All of the data is anonymized and cannot be used to identify you',
                                    toggle: {
                                        value: 'targeting',
                                        enabled: false,
                                        readonly: false
                                    }
                                }, {
                                    title: 'Více informací',
                                    description: 'For any queries in relation to our policy on cookies and your choices, please <a class="cc-link" href="#yourcontactpage">contact us</a>.',
                                }
                            ]
                        }
                    },
                    'cs': {
                        consent_modal: {
                            title: 'Toxin používá Cookies!',
                            description: 'Aby Toxin web fungoval správně, využíváme nezbytná cookies. Jestli se o nich chcete dozvědět víc nebo si navolit dobrovolná cookies, <button type="button" data-cc="c-settings" class="cc-link">klikněte zde</button>',
                            primary_btn: {
                                text: 'Přijímám vše',
                                role: 'accept_all'              // 'accept_selected' or 'accept_all'
                            },
                            secondary_btn: {
                                text: 'Odmítám vše',
                                role: 'accept_necessary'        // 'settings' or 'accept_necessary'
                            }
                        },
                        settings_modal: {
                            title: 'COOKIES NASTAVENÍ',
                            save_settings_btn: 'Uložit nastavení',
                            accept_all_btn: 'Přijímám vše',
                            reject_all_btn: 'Odmítám vše',
                            close_btn_label: 'Close',
                            cookie_table_headers: [
                                {col1: 'Jméno'},
                                {col2: 'Doména'},
                                {col3: 'Expirace'},
                                {col4: 'Popis'}
                            ],
                            blocks: [
                                {
                                    title: 'Využití cookies',
                                    description: 'Cookies používáme nejen k zabezpečení základních funkcí webu, ale taky abychom vám poskytli tu nejlepší uživatelskou zkušenost a oslovovali vás vhodnými reklamami. Zde si můžete zvolit jednotlivé kategorie a nastavit, jak máme s vašimi daty pracovat.'
                                }, {
                                    title: 'Nezbytná základní cookies',
                                    description: 'Tato cookies jsou nezbytná, neboť vám umožňují používat stránku. Tato kategorie nemůže být vypnuta.',
                                    toggle: {
                                        value: 'necessary',
                                        enabled: true,
                                        readonly: true          // cookie categories with readonly=true are all treated as "necessary cookies"
                                    }
                                }, {
                                    title: 'Funkční a analytické cookies',
                                    description: 'Tato cookies dovolují stránce zapamatovat si volby, které jste v minulosti udělali. ',
                                    toggle: {
                                        value: 'analytics',     // your cookie category
                                        enabled: false,
                                        readonly: false
                                    },
                                    cookie_table: [             // list of all expected cookies
                                        {
                                            col1: '^_ga',       // match all cookies starting with "_ga"
                                            col2: 'google.com',
                                            col3: '1 rok',
                                            col4: 'Google analytics pro sledování návštěvnosti',
                                            is_regex: true
                                        }
                                    ]
                                }, {
                                    title: 'Cookies pro cílení a reklamu',
                                    description: 'Tato cookies sbírají informace o tom, jak využíváte Toxin web. Pamatují si, že jste ho navštívili a na které stránky jste klikli. Všechna data jsou anonymizována a není možné vás na jejich základě identifikovat. ',
                                    toggle: {
                                        value: 'targeting',
                                        enabled: false,
                                        readonly: false
                                    }
                                }, {
                                    title: 'Více informací',
                                    description: 'V případě jiných dotazů ohledně využívání cookies nás neváhejte nás <a class="cc-link" href="#kontakt">kontaktovat</a>.',
                                }
                            ]
                        }
                    }
                }
            });
        });
    </script>
</html>
