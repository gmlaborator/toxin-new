<?php if ($root->getDepth() === 0) : ?>
<ul class="uk-navbar-nav">

<?php endif ?>

<li class="uk-parent" >
    <a href="/#maximus">Monitoring médií Maximus</a>
</li>

<li class="uk-parent" >
    <a href="/#analyza_monitoringu_medii">Analýza monitoringu médií</a>
</li>

<li class="uk-parent" >
    <a href="/#oNas">O nás</a>
</li>
<li class="uk-parent" >
    <a href="/#kontakt">Kontakt</a>
</li>

<li class="uk-parent" >
    <a href="/blog" class="active">Blog</a>
</li>
<li class="uk-parent" >
    <a href="https://www.maximusweb.cz/" class="navbar-toxin-menu-button">Přihlásit do Maximu</a>
</li>

<?php if ($root->getDepth() === 0) : ?>
</ul>
<?php endif ?>
