<?php $view->script('posts', 'blog:app/bundle/posts.js', 'vue') ?>

<div class="tm-container-large">

    <?php foreach ($posts as $post) : ?>
      <?php if ($image = $post->get('image.src')){
        $class = "uk-flex";
      }else{
        $class = "";
      }?>
    <article class="uk-article <?=$class?>">
        <?php if ($image = $post->get('image.src')): ?>
      <div class="uk-article-left">
        <a class="uk-display-block" href="<?= $view->url('@blog/id', ['id' => $post->id]) ?>"><img src="<?= $image ?>" alt="<?= $post->get('image.alt') ?>"></a>
      </div>
      <div  class="uk-article-right">
            <?php endif ?>
        <h2 class="uk-article-title"><a href="<?= $view->url('@blog/id', ['id' => $post->id]) ?>"><?= $post->title ?></a></h2>

        <p class="uk-article-meta">
            <?= __(' %date%', ['%name%' => $post->user->name, '%date%' => '<time datetime="'.$post->date->format(\DateTime::W3C).'" v-cloak>{{ "'.$post->date->format(\DateTime::W3C).'" | date "longDate" }}</time>' ]) ?>
        </p>
        <div class="uk-margin"><?= $post->excerpt ?: $post->content ?></div>
        <a style="color:#27348b" href="<?= $view->url('@blog/id', ['id' => $post->id]) ?>">Číst dále</a>
              <?php if ($image = $post->get('image.src')): ?>
      </div>
    <?php endif ?>
    </article>
    <?php endforeach ?>

    <?php
echo "<script>console.log('Počet total: $total')</script>";
        $range     = 3;
        $total     = intval($total);
        $page      = intval($page);
        $pageIndex = $page - 1;

    ?>

    <?php if ($total > 1) : ?>
    <ul class="uk-pagination">


        <?php for($i=1;$i<=$total;$i++): ?>
            <?php if ($i <= ($pageIndex+$range) && $i >= ($pageIndex-$range)): ?>

                <?php if ($i == $page): ?>
                <li class="uk-active"><span><?=$i?></span></li>
                <?php else: ?>
                <li>
                    <a href="<?= $view->url('@blog/page', ['page' => $i]) ?>"><?=$i?></a>
                <li>
                <?php endif; ?>

            <?php elseif($i==1): ?>

                <li>
                    <a href="<?= $view->url('@blog/page', ['page' => 1]) ?>">1</a>
                </li>
                <li><span>...</span></li>

            <?php elseif($i==$total): ?>

                <li><span>...</span></li>
                <li>
                    <a href="<?= $view->url('@blog/page', ['page' => $total]) ?>"><?=$total?></a>
                </li>

            <?php endif; ?>
        <?php endfor; ?>


    </ul>
    <?php endif ?>

</div>
