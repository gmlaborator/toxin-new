$(document).ready(function() {
    //nicescroll body


 window.dataLayer = window.dataLayer || [];
 window.dataLayer.push({
 'event': 'formSent'
 });


    /*
    if ($(window).width() >= 768) {
    $('.scroll-bar').niceScroll({
        background: '#bbb',
        cursorborder: '0',
        cursorwidth: '4px',
        horizrailenabled: false,
        cursorfixedheight: 400,
        mousescrollstep: 60,
    });

    $('.scroll-bar').mouseover(function() {
        $('.scroll-bar').getNiceScroll().resize();
    });
    }
    */




    if ($(window).width() >= 768) {
        $('.scroll-bar').niceScroll({
            background: '#bbb',
            cursorborder: '0',
            cursorwidth: '4px',
            horizrailenabled: false,
            cursorfixedheight: 400,
            mousescrollstep: 60,
        });

        $('.scroll-bar').mouseover(function() {
            $('.scroll-bar').getNiceScroll().resize();
        });

        //nicescroll body
        jQuery("html").niceScroll({
            cursorcolor: '#040e52',
            cursorborder: '4px solid #000',
            cursoropacitymin: '0',
            cursoropacitymax: '1',
            cursorwidth: '4px',
            cursorfixedheight: 400,
            mousescrollstep: 60,
            zindex: 99999999,

        });
    }




    /* custom settings */
    /*
   $('.venobox').venobox({
    //  framewidth: '400px',        // default: ''
    //   frameheight: '300px',       // default: ''
    //  border: '10px',             // default: '0'
    //   bgcolor: '#5dff5e',         // default: '#fff'
    titleattr: 'data-title',    // default: 'title'
    numeratio: false,            // default: false
    infinigall: true,            // default: false
    spinner: 'spinner-pulse',
});
*/

    // response hamburger
    $(document).on('click', function(e) {
        if ($(e.target).closest('#js-hamburger').length) {
            $(".navigace__ul").addClass('responze-nav');
            $(".responze-nav").show();
            $(".hamburger").hide();
        } else if ($(e.target).closest('.responze-nav li a').length) {
            $(".navigace__ul").removeClass('.responze-nav');
            $(".responze-nav").hide();
            $(".hamburger").show();
        } else if ($(e.target).closest('a#zavrit').length) {
            $(".navigace__ul").removeClass('.responze-nav');
            $(".responze-nav").hide();
            $(".hamburger").show();
        } else if (!$(e.target).closest('#hamburger').length) {
            $(".navigace__ul").removeClass('.responze-nav');
            $(".responze-nav").hide();
        }

        $(".zavrit").on("click", function() {
            $("#responze").hide();
        });
    });






    $(".formular").animatedModal({
        modalTarget: 'js-popup',
        animatedIn: 'lightSpeedIn',
        animatedOut: 'bounceOutDown',
        // Callbacks
        beforeOpen: function() {
            // console.log("The animation was called");
        },

        afterOpen: function() {
            //  console.log("The animation is completed");
        },


        beforeClose: function() {
            // console.log("The animation was called");

            if ($(window).width() >= 768) {

            $("body").css({
                overflow: 'hidden'
            });
            $("body").css({
                position: 'relative'
            });
            $("html").css({
                overflow: 'hidden'
            });

        }


        },





        afterClose: function() {
            //   console.log("The animation is completed");
        }
    });



    function restartErrorDisplay() {
        document.getElementById("display-error-box").innerHTML = "";
        var inputsIDs = ["email", "jmeno", "telefon", "poznamka"];
        for (var i = 0; i < inputsIDs.length; i++) {
            id = inputsIDs[i];
            document.getElementById(id).style.borderBottomColor = "#27348b";
            document.getElementById(id).style.color = "lightgray";
        }
    }

    function getValuesForm(valuesData) {
        var inputsIDs = ["email", "jmeno", "telefon", "poznamka"];
        for (var i = 0; i < inputsIDs.length; i++) {
            var idElem = inputsIDs[i];
            valuesData[idElem] = document.getElementById(idElem).value;
            if (valuesData[idElem] == "") {
                document.getElementById(idElem).style.borderBottomColor = "red";
                document.getElementById(idElem).style.color = "red";
                valuesData["odeslat"] = false;
            }
        }
        return valuesData;
    }

    $("#odeslatFormularEmail").on('click', function(e) {
        grecaptcha.ready(function() {
            grecaptcha.execute('6LeUg8MUAAAAANNNxn90gY7IS-l9CaxNhroGf4Ni').then(function(token) {
                $("#odeslatFormularEmail").prop('disabled', true);
                restartErrorDisplay();
                var valuesData = {
                    "email": "",
                    "jmeno": "",
                    "telefon": "",
                    "poznamka": "",
                    "odeslat": true,
                    "subjectType": 2,
                };
                var formData = getValuesForm(valuesData);
                var tag = "page";
                if (formData["odeslat"]) {
                    $.ajax({
                        type: "POST",
                        url: "req/ajax/send-mail.php",
                        data: {
                            "email": formData["email"],
                            "telefon": formData["telefon"],
                            "poznamka": formData["poznamka"],
                            "tag": tag,
                            "jmeno": formData["jmeno"],
                            "token": token,
                            "subjectType": formData["subjectType"]
                        },
                        success: function(data) {
                          console.log(data);
                            var status = JSON.parse(data);
                            if(status["class"] == "success"){
                              displayPopup(status["class"], status["message"]);
                              $('#message-popup-ajax-' + status["class"]).delay(3000).fadeOut('slow');
                            }else{
                                document.getElementById("display-error-box").innerHTML = status["message"];
                            }
                            $("#odeslatFormularEmail").prop('disabled', false);
                            var inputsIDs = ["email", "jmeno", "telefon", "poznamka"];
                            clearForm(inputsIDs);
                        },
                        error: function(jqXHR, textStatus, errorThrown, data) {
                            // error handling
                        }
                    });
                } else {
                    document.getElementById("display-error-box").innerText = "Všechna pole jsou povinná";
                }
            });
        });
    });


    function restartErrorDisplayPopup() {
        document.getElementById("display-error-box-popup").innerHTML = "";
        var inputsIDs = ["email", "jmeno", "telefon"];
        for (var i = 0; i < inputsIDs.length; i++) {
            id = inputsIDs[i];
            document.getElementById("popup-" + id).style.borderBottomColor = "#27348b";
            document.getElementById("popup-" + id).style.color = "lightgray";
        }
    }

    function clearForm(inputs) {
        for (var i = 0; i < inputs.length; i++) {
            id = inputs[i];
            document.getElementById(id).value = "";
        }
    }

    function getValuesFormPopup(valuesData) {
        var inputsIDs = ["email", "jmeno", "telefon", "poznamka", "tagPopup" ];
        for (var i = 0; i < inputsIDs.length; i++) {
            var idElem = inputsIDs[i];
            valuesData[idElem] = document.getElementById("popup-" + idElem).value;
            if (valuesData[idElem] == "") {
                document.getElementById("popup-" + idElem).style.borderBottomColor = "red";
                document.getElementById("popup-" + idElem).style.color = "red";
                valuesData["odeslat"] = false;
            }
        }
        return valuesData;
    }




    $("#popup-odeslatFormularEmail").on('click', function(e) {

        grecaptcha.ready(function() {
            grecaptcha.execute('6LeUg8MUAAAAANNNxn90gY7IS-l9CaxNhroGf4Ni').then(function(token) {
                $("#popup-odeslatFormularEmail").prop('disabled', true);
                restartErrorDisplayPopup();
                var valuesData = {
                    "email": "",
                    "jmeno": "",
                    "telefon": "",
                    "poznamka": "",
                    "odeslat": true,
                    "tagPopup": ""
                };
                var formData = getValuesFormPopup(valuesData);
                var tag = "popup";



               // var poznamka = 0;
                if (formData["odeslat"]) {
                    $.ajax({
                        type: "POST",
                        url: "req/ajax/send-mail.php",
                        data: {
                            "email": formData["email"],
                            "telefon": formData["telefon"],
                            "poznamka": formData["poznamka"],
                            "tag": tag,
                            "subjectType": formData["tagPopup"],
                            "jmeno": formData["jmeno"],
                            "token": token
                        },
                        success: function(data) {
                            console.log("Výsledek mailu: "+data);
                            var status = JSON.parse(data);
                            if(status["class"] == "success"){
                              displayPopup(status["class"], status["message"]);
                              $('#message-popup-ajax-' + status["class"]).delay(3000).fadeOut('slow');
                            }else{
                                document.getElementById("display-error-box-popup").innerHTML = status["message"];
                            }


                            $("#popup-odeslatFormularEmail").prop('disabled', false);
                            var inputsIDs = ["popup-email", "popup-jmeno", "popup-telefon", "popup-poznamka" ];
                            clearForm(inputsIDs);




                            $('#js-popup').attr('class', 'popup_formular js-popup-off bounceOutDown animated');
                            $("html").css({ 'overflow': "auto" });
                            $("body").css({ 'overflow': "auto" });

                            if ($(window).width() >= 768) {
                                $("body").css({overflow: 'hidden' });
                                $("body").css({ position: 'relative' });
                                $("html").css({   overflow: 'hidden' });
                            }





                        },
                        error: function(jqXHR, textStatus, errorThrown, data) {
                            // error handling
                        }
                    });
                } else {
                    document.getElementById("display-error-box-popup").innerText = "Všechna pole jsou povinná";
                }
            });
        });
    });







    function displayPopup(className, text) {
        document.getElementById("message-popup-ajax-" + className).style.display = "block";
        if (className != "success") {
            document.getElementById("error-text").innerHTML = text;
        }
    }




    $(document).scroll(function() {
        var scroll_top = $(document).scrollTop();

        var div_one_top = $('#maximus').position().top;
        var div_two_top = $('#oNas').position().top;
        var div_three_top = $('#kontakt').position().top;

        if (scroll_top > div_one_top && scroll_top < div_two_top && scroll_top < div_three_top) {
            document.getElementById("navigace-prvni").classList.remove("active");
            document.getElementById("navigace-druhe").classList.add("active");
            document.getElementById("navigace-treti").classList.remove("active");
        } else if (scroll_top > div_two_top && scroll_top < div_three_top) {
            document.getElementById("navigace-prvni").classList.remove("active");
            document.getElementById("navigace-druhe").classList.add("active");
            document.getElementById("navigace-treti").classList.remove("active");
        } else if (scroll_top > div_three_top) {
            document.getElementById("navigace-prvni").classList.remove("active");
            document.getElementById("navigace-druhe").classList.remove("active");
            document.getElementById("navigace-treti").classList.add("active");
        }

    });




    // response hamburger
    $(document).on('click', function(e) {
        if ($(e.target).closest('#hamburger').length) {
            $(".navigace__ul").addClass('responze-nav');
            $(".responze-nav").show();
            $(".hamburger").hide();
        } else if ($(e.target).closest('.responze-nav li a').length) {
            $(".navigace__ul").removeClass('.responze-nav');
            $(".responze-nav").hide();
            $(".hamburger").show();
        } else if ($(e.target).closest('a#zavrit').length) {
            $(".navigace__ul").removeClass('.responze-nav');
            $(".responze-nav").hide();
            $(".hamburger").show();
        }
        /*
         else if ( ! $(e.target).closest('#hamburger').length ) {
         $(".navigace__ul").removeClass('.responze-nav');
         $(".responze-nav").hide();
         }
         */
        $(".zavrit").on("click", function() {
            $("#responze").hide();
        });
    });



    /*
    if ($(window).width() >= 768) {
    $('.scroll-bar').niceScroll({
        background: '#bbb',
        cursorborder: '0',
        cursorwidth: '4px',
        horizrailenabled: false,
        cursorfixedheight: 400,
        mousescrollstep: 60,
    });

    $('.scroll-bar').mouseover(function() {
        $('.scroll-bar').getNiceScroll().resize();
    });
    }
    */




    // posun při kliku na navigaci
    $(".navigace__ul a, .menu__inside__logo, .firstBlock__inside__left__button").click(function(e) {
        var $this = $(this);
        var a = $(this).attr("href");

        var top = $($this.attr('href')).offset().top - 60;
        /*   }     */
        $('html, body').stop().animate({
            scrollTop: top
        }, '2000');
        e.preventDefault();
    });




});




$(document).ready(
    function() {
        /* custom settings */
        $('.venobox').venobox({
              framewidth: '90%',        // default: ''
            //   frameheight: '300px',       // default: ''
            //  border: '10px',             // default: '0'
            //   bgcolor: '#5dff5e',         // default: '#fff'
            titleattr: 'data-title', // default: 'title'
            numeratio: false, // default: false
            infinigall: true, // default: false
            spinner: 'spinner-pulse',
        });

    });


function setTag(tag){
  document.getElementById("popup-tagPopup").value = tag;
}
